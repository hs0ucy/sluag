#!/usr/bin/env lua
--- Utility for creating URL slugs from strings.
--- No dependency, you only need lua (>= 5.1).
-- @name sluag
-- @param [-i] Pass the string directly to sluag.
-- @param [-s] Choose a custom separator (default is '-')
-- @param [-l] Limit the number of characters (default is unlimited)
-- @return A slug
-- Usage
--[[
  $ echo "Le Cortège du Bœuf gras passant place de la Concorde" | ./sluag
  le-cortege-du-boeuf-gras-passant-place-de-la-concorde

  $ echo "Le Cortège du Bœuf gras passant place de la Concorde" | ./sluag -s /
  le/cortege/du/boeuf/gras/passant/place/de/la/concorde

  $ echo "Le Cortège du Bœuf gras passant place de la Concorde" | ./sluag -l 20
  le-cortege-du-boeuf

  $ head -n 1 example.txt | ./sluag
  georges-melies-filmography

  $ ./sluag < example.txt
  georges-melies-filmography
  la-lune-a-un-metre-1-lobservatoire-2-la-lune-3-phoebe
  smarter-than-the-teacher
  arrival-of-a-train-at-vincennes-station
  ultima-syriarum-est-palaestina-per-intervalla
  coronation-of-a-village-maiden
  steamboats-on-river-seine
  place-de-lopera-2d
  jetee-et-plage-de-trouville-1re-partie

  $ ./sluag -i "Le Cortège du Bœuf gras passant place de la Concorde
  le-cortege-du-boeuf-gras-passant-place-de-la-concorde
]]

local charmap = {
  -- Latin
  ["À"] = "A", ["Á"] = "A", ["Â"] = "A", ["Ã"] = "A", ["Ä"] = "A", ["Å"] = "A", ["Æ"] = "AE", ["Ç"] = "C",
  ["È"] = "E", ["É"] = "E", ["Ê"] = "E", ["Ë"] = "E", ["Ì"] = "I", ["Í"] = "I", ["Î"] = "I", ["Ï"] = "I",
  ["Ð"] = "D", ["Ñ"] = "N", ["Ò"] = "O", ["Ó"] = "O", ["Ô"] = "O", ["Õ"] = "O", ["Ö"] = "O", ["Ő"] = "O",
  ["Ø"] = "O", ["Ù"] = "U", ["Ú"] = "U", ["Û"] = "U", ["Ü"] = "U", ["Ű"] = "U", ["Ý"] = "Y", ["Þ"] = "TH",
  ["ß"] = "ss",
  ["à"] = "a", ["á"] = "a", ["â"] = "a", ["ã"] = "a", ["ä"] = "a", ["å"] = "a", ["æ"] = "ae", ["ç"] = "c",
  ["è"] = "e", ["é"] = "e", ["ê"] = "e", ["ë"] = "e", ["ì"] = "i", ["í"] = "i", ["î"] = "i", ["ï"] = "i",
  ["ð"] = "d", ["ñ"] = "n", ["ò"] = "o", ["ó"] = "o", ["ô"] = "o", ["õ"] = "o", ["ö"] = "o", ["ő"] = "o",
  ["ø"] = "o", ["ù"] = "u", ["ú"] = "u", ["û"] = "u", ["ü"] = "u", ["ű"] = "u", ["ý"] = "y", ["þ"] = "th",
  ["ÿ"] = "y",
  -- Latin symbols
  ["©"] = "(c)",
  -- Greek
  ["Α"] = "A", ["Β"] = "B", ["Γ"] = "G", ["Δ"] = "D", ["Ε"] = "E", ["Ζ"] = "Z", ["Η"] = "H", ["Θ"] = "8",
  ["Ι"] = "I", ["Κ"] = "K", ["Λ"] = "L", ["Μ"] = "M", ["Ν"] = "N", ["Ξ"] = "3", ["Ο"] = "O", ["Π"] = "P",
  ["Ρ"] = "R", ["Σ"] = "S", ["Τ"] = "T", ["Υ"] = "Y", ["Φ"] = "F", ["Χ"] = "X", ["Ψ"] = "PS", ["Ω"] = "W",
  ["Ά"] = "A", ["Έ"] = "E", ["Ί"] = "I", ["Ό"] = "O", ["Ύ"] = "Y", ["Ή"] = "H", ["Ώ"] = "W", ["Ϊ"] = "I",
  ["Ϋ"] = "Y",
  ["α"] = "a", ["β"] = "b", ["γ"] = "g", ["δ"] = "d", ["ε"] = "e", ["ζ"] = "z", ["η"] = "h", ["θ"] = "8",
  ["ι"] = "i", ["κ"] = "k", ["λ"] = "l", ["μ"] = "m", ["ν"] = "n", ["ξ"] = "3", ["ο"] = "o", ["π"] = "p",
  ["ρ"] = "r", ["σ"] = "s", ["τ"] = "t", ["υ"] = "y", ["φ"] = "f", ["χ"] = "x", ["ψ"] = "ps", ["ω"] = "w",
  ["ά"] = "a", ["έ"] = "e", ["ί"] = "i", ["ό"] = "o", ["ύ"] = "y", ["ή"] = "h", ["ώ"] = "w", ["ς"] = "s",
  ["ϊ"] = "i", ["ΰ"] = "y", ["ϋ"] = "y", ["ΐ"] = "i",
  -- Turkish
  ["Ş"] = "S", ["İ"] = "I", ["Ç"] = "C", ["Ü"] = "U", ["Ö"] = "O", ["Ğ"] = "G",
  ["ş"] = "s", ["ı"] = "i", ["ç"] = "c", ["ü"] = "u", ["ö"] = "o", ["ğ"] = "g",
  -- Russian
  ["А"] = "A", ["Б"] = "B", ["В"] = "V", ["Г"] = "G", ["Д"] = "D", ["Е"] = "E", ["Ё"] = "Yo", ["Ж"] = "Zh",
  ["З"] = "Z", ["И"] = "I", ["Й"] = "J", ["К"] = "K", ["Л"] = "L", ["М"] = "M", ["Н"] = "N", ["О"] = "O",
  ["П"] = "P", ["Р"] = "R", ["С"] = "S", ["Т"] = "T", ["У"] = "U", ["Ф"] = "F", ["Х"] = "H", ["Ц"] = "C",
  ["Ч"] = "Ch", ["Ш"] = "Sh", ["Щ"] = "Sh", ["Ъ"] = "", ["Ы"] = "Y", ["Ь"] = "", ["Э"] = "E", ["Ю"] = "Yu",
  ["Я"] = "Ya",
  ["а"] = "a", ["б"] = "b", ["в"] = "v", ["г"] = "g", ["д"] = "d", ["е"] = "e", ["ё"] = "yo", ["ж"] = "zh",
  ["з"] = "z", ["и"] = "i", ["й"] = "j", ["к"] = "k", ["л"] = "l", ["м"] = "m", ["н"] = "n", ["о"] = "o",
  ["п"] = "p", ["р"] = "r", ["с"] = "s", ["т"] = "t", ["у"] = "u", ["ф"] = "f", ["х"] = "h", ["ц"] = "c",
  ["ч"] = "ch", ["ш"] = "sh", ["щ"] = "sh", ["ъ"] = "", ["ы"] = "y", ["ь"] = "", ["э"] = "e", ["ю"] = "yu",
  ["я"] = "ya",
  -- Ukrainian
  ["Є"] = "Ye", ["І"] = "I", ["Ї"] = "Yi", ["Ґ"] = "G",
  ["є"] = "ye", ["і"] = "i", ["ї"] = "yi", ["ґ"] = "g",
  -- Czech
  ["Č"] = "C", ["Ď"] = "D", ["Ě"] = "E", ["Ň"] = "N", ["Ř"] = "R", ["Š"] = "S", ["Ť"] = "T", ["Ů"] = "U",
  ["Ž"] = "Z",
  ["č"] = "c", ["ď"] = "d", ["ě"] = "e", ["ň"] = "n", ["ř"] = "r", ["š"] = "s", ["ť"] = "t", ["ů"] = "u",
  ["ž"] = "z",
  -- Polish
  ["Ą"] = "A", ["Ć"] = "C", ["Ę"] = "e", ["Ł"] = "L", ["Ń"] = "N", ["Ó"] = "o", ["Ś"] = "S", ["Ź"] = "Z",
  ["Ż"] = "Z",
  ["ą"] = "a", ["ć"] = "c", ["ę"] = "e", ["ł"] = "l", ["ń"] = "n", ["ó"] = "o", ["ś"] = "s", ["ź"] = "z",
  ["ż"] = "z",
  -- Latvian
  ["Ā"] = "A", ["Č"] = "C", ["Ē"] = "E", ["Ģ"] = "G", ["Ī"] = "i", ["Ķ"] = "k", ["Ļ"] = "L", ["Ņ"] = "N",
  ["Š"] = "S", ["Ū"] = "u", ["Ž"] = "Z",
  ["ā"] = "a", ["č"] = "c", ["ē"] = "e", ["ģ"] = "g", ["ī"] = "i", ["ķ"] = "k", ["ļ"] = "l", ["ņ"] = "n",
  ["š"] = "s", ["ū"] = "u", ["ž"] = "z",
  -- -- Varia
  ["'"] = "",
  ["@"] = "",
  ["*"] = "",
  ["$"] = "",
  ["œ"] = "oe",
}

do
  local str

  -- Check if a table has a specific value
  local hasvalue = function(tbl, val)
    for k, value in ipairs(tbl) do
      if value == val then
        return k
      end
    end

    return false
  end

  -- Create the slug
  local slugify = function(s)
    local limit
    local separator = '-'
    local words = {}

    for k, _ in pairs(charmap) do
      s = s:gsub(tostring(k), charmap[k])
    end

    -- Apply the limit if applicable
    if arg[hasvalue(arg, '-l')] and tonumber(arg[hasvalue(arg, '-l')+1]) then
      limit = tonumber(arg[hasvalue(arg, '-l')+1])
      s = s:sub(1, limit)
    else
      s = s
    end

    -- Put lowercase words into a table
    -- And set them to lowercase
    for word in s:gmatch("%w+") do
      words[#words+1] = word:lower()
    end

    -- Apply a custom separator if applicable
    if arg[hasvalue(arg, '-s')] and type(arg[hasvalue(arg, '-s')+1]) == 'string' then
      separator = arg[hasvalue(arg, '-s')+1]
    end

    -- Concatenate with the separator
    return table.concat(words, separator)
  end

  -- Get the string to slugify
  if arg[hasvalue(arg, '-i')] and tostring(arg[hasvalue(arg, '-i')+1]) then
    str = tostring(arg[hasvalue(arg, '-i')+1])
    print(slugify(str))
  else
    for line in (io.lines()) do
      print(slugify(line))
    end
  end

  return
end
