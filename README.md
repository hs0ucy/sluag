# sluag

Command-line utility for creating URL slugs from strings. Without any
dependency, you only need lua (>= 5.1).

Redirect a file with strings in it or pipe a string by stdin.

* `param [-i] Pass the string directly to sluag`.
* `param [-s] Choose a custom separator (default is '-')`.
* `param [-l] Limit the number of characters (default is unlimited)`.

Usage:

    $ echo "Le Cortège du Bœuf gras passant place de la Concorde" | ./sluag
    le-cortege-du-boeuf-gras-passant-place-de-la-concorde

    $ echo "Le Cortège du Bœuf gras passant place de la Concorde" | ./sluag -s /
    le/cortege/du/boeuf/gras/passant/place/de/la/concorde

    $ echo "Le Cortège du Bœuf gras passant place de la Concorde" | ./sluag -l 20
    le-cortege-du-boeuf

    $ head -n 1 example.txt | ./sluag
    georges-melies-filmography

    $ ./sluag < example.txt
    georges-melies-filmography
    la-lune-a-un-metre-1-lobservatoire-2-la-lune-3-phoebe
    smarter-than-the-teacher
    arrival-of-a-train-at-vincennes-station
    ultima-syriarum-est-palaestina-per-intervalla
    coronation-of-a-village-maiden
    steamboats-on-river-seine
    place-de-lopera-2d
    jetee-et-plage-de-trouville-1re-partie

    $ ./sluag -i "Le Cortège du Bœuf gras passant place de la Concorde
    le-cortege-du-boeuf-gras-passant-place-de-la-concorde

---

To have access to `sluag` from everywhere, you can create a symlink
in `$HOME/.local/bin/` or in `/usr/local/bin/`.

Todo
----

* Have an option to add a suffix like a file extension. Ex.: `.html`.
